import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { LandingComponent } from './landing/landing.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
    declarations: [
        LandingComponent
    ],
    imports: [
        CommonModule,
        MatButtonModule,
        MatIconModule,
        HomeRoutingModule,
        FlexLayoutModule
    ]
})
export class HomeModule { }
