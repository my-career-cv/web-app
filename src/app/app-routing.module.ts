import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LandingComponent } from "./home/landing/landing.component";
import { TemplateComponent } from "./editor/template/template.component";
import { DashboardComponent } from "./dash/dashboard/dashboard.component";
import { EnhancerComponent } from "./editor/enhancer/enhancer.component";

const routes: Routes = [
    {
        path: "home",
        children: [
            {
                path: "",
                component: LandingComponent
            }
        ]
    },
    {
        path: "editor",
        children: [
            {
                path: "template",
                component: TemplateComponent
            },
            {
                path: "enhancer",
                component: EnhancerComponent
            }
        ]
    },
    {
        path: "dash",
        children: [
            {
                path: "board",
                component: DashboardComponent
            }
        ]
    },
    {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            { enableTracing: true } // debugging purposes only
        )
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {}
