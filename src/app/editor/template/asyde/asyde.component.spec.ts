import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsydeComponent } from './asyde.component';

describe('AsydeComponent', () => {
  let component: AsydeComponent;
  let fixture: ComponentFixture<AsydeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsydeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsydeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
