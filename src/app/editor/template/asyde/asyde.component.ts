import { Component, OnInit, Input } from "@angular/core";
import {
    Personal,
    Summary,
    Experience,
    Skills
} from "../../interfaces/section";
import { Education } from "../../interfaces/section";

@Component({
    selector: "template-asyde",
    templateUrl: "./asyde.component.html",
    styleUrls: ["./asyde.component.scss"]
})
export class AsydeComponent implements OnInit {
    @Input("sections") sections: any;
    personal: Personal;
    education: Education;
    experience: Experience;
    skills: Skills;
    summary: Summary;
    constructor() {}

    ngOnInit(): void {
        console.log("Asyde component", this.sections);
        this.personal = this.sections.find(
            obj => obj.header === "Personal Info"
        );
        this.education = this.sections.find(obj => obj.header === "Education");
        this.experience = this.sections.find(
            obj => obj.header === "Experience"
        );
        this.skills = this.sections.find(obj => obj.header === "Skills");
        this.summary = this.sections.find(obj => obj.header === "Summary");
        console.log("personal object is ", this.personal);
        console.log("education object is ", this.education);
        console.log("experience object is ", this.experience);
        console.log("skills object is ", this.skills);
        console.log("summary object is ", this.summary);
    }
}
