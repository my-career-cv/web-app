import { Component, OnInit, Input } from "@angular/core";
import { Experience } from "../../interfaces/section";

@Component({
    selector: "edit-experience",
    templateUrl: "./experience.component.html",
    styleUrls: ["./experience.component.scss"]
})
export class ExperienceComponent implements OnInit {
    @Input("experience") experience: Experience;
    constructor() {}

    addPosition(): void {
        const positionNumber = this.experience.items.length + 1;
        this.experience.items.push({
            header: "Position " + positionNumber,
            employer: "",
            city: "",
            designation: "",
            startDate: "",
            endDate: "",
            description: "",
            checked: false
        });
        console.log("experience items ", this.experience);
    }

    deletePosition(index: number): void {
        this.experience.items.splice(index, 1);
        console.log("experience items ", this.experience);
    }

    onGoingPosition(item, $event): void {
        console.log("school items ", item);
        console.log("mat check event ", $event);
        if ($event.checked) {
            item.checked = true;
            item.endDate = "Present";
        } else {
            console.log("checked is false");
            item.checked = false;
            item.endDate = "";
        }
    }

    ngOnInit(): void {}
}
