import { Component, OnInit, Input } from "@angular/core";
import { Summary } from "../../interfaces/section";

@Component({
    selector: "edit-summary",
    templateUrl: "./summary.component.html",
    styleUrls: ["./summary.component.scss"]
})
export class SummaryComponent implements OnInit {
    @Input("summary") summary: Summary;
    constructor() {}

    ngOnInit(): void {}
}
