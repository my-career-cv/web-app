import { Component, OnInit, Input, OnDestroy } from "@angular/core";
import { Section } from "../interfaces/section";
import { Subscription } from "rxjs";
import { MediaObserver, MediaChange } from "@angular/flex-layout";
import { MatTabChangeEvent } from "@angular/material/tabs";

@Component({
    selector: "app-edit",
    templateUrl: "./edit.component.html",
    styleUrls: ["./edit.component.scss"]
})
export class EditComponent implements OnInit, OnDestroy {
    @Input("sections") sections: any;
    maxNumberOfTabs: number;
    selectedTab: number = 0;
    private mqWatcher: Subscription;
    constructor(private mediaObserver: MediaObserver) {}

    tabChanged(tabChangeEvent: MatTabChangeEvent): void {
        this.selectedTab = tabChangeEvent.index;
    }

    back(): void {
        console.log(this.selectedTab);
        if (this.selectedTab != 0) {
            this.selectedTab = this.selectedTab - 1;
        }
    }

    next(): void {
        console.log(this.selectedTab);
        this.maxNumberOfTabs = this.sections.length - 1;
        if (this.selectedTab !== this.maxNumberOfTabs) {
            this.selectedTab = this.selectedTab + 1;
        }
    }

    ngOnInit(): void {
        this.maxNumberOfTabs = this.sections.length - 1;
        this.mqWatcher = this.mediaObserver.media$.subscribe(
            (change: MediaChange) => {
                console.log(change);
                console.log(change.mqAlias);
            }
        );
    }

    ngOnDestroy(): void {
        if (this.mqWatcher) {
            this.mqWatcher.unsubscribe();
        }
    }
}
