import { Component, OnInit, Input } from "@angular/core";

@Component({
    selector: "edit-personal",
    templateUrl: "./edit-personal.component.html",
    styleUrls: ["./edit-personal.component.scss"]
})
export class EditPersonalComponent implements OnInit {
    // properties is set to any for now
    @Input("personal") personal;
    constructor() {}

    ngOnInit(): void {}
}
