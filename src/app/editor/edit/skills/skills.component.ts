import { Component, OnInit, Input, ViewChild, ElementRef } from "@angular/core";
import { Skills } from "../../interfaces/section";

@Component({
    selector: "edit-skills",
    templateUrl: "./skills.component.html",
    styleUrls: ["./skills.component.scss"]
})
export class SkillsComponent implements OnInit {
    @Input("skills") skills: Skills;
    @ViewChild("skillset") skillset: ElementRef;
    constructor() {}

    addSkill(skill: string): void {
        this.skills.items.push(skill);
        console.log("skills are ", this.skills.items);
        this.skillset.nativeElement.value = "";
    }

    deleteSkill(index: number): void {
        this.skills.items.splice(index, 1);
        console.log("skills are ", this.skills.items);
    }

    ngOnInit(): void {}
}
