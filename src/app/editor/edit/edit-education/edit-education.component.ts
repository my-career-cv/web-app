import { Component, OnInit, Input } from "@angular/core";
import { Education } from "../../interfaces/section";

@Component({
    selector: "edit-education",
    templateUrl: "./edit-education.component.html",
    styleUrls: ["./edit-education.component.scss"]
})
export class EditEducationComponent implements OnInit {
    @Input("education") education: Education;
    constructor() {}

    addSchool(): void {
        const schoolNumber = this.education.items.length + 1;
        this.education.items.push({
            header: "School " + schoolNumber,
            schoolName: "",
            city: "",
            degree: "",
            fieldOfStudy: "",
            grade: "",
            startDate: "",
            graduationDate: "",
            checked: false
        });
        console.log("education items ", this.education);
    }

    deleteDegree(index: number): void {
        this.education.items.splice(index, 1);
        console.log("education items ", this.education);
    }

    onGoingSchool(item, $event): void {
        console.log("school items ", item);
        console.log("mat check event ", $event);
        if ($event.checked) {
            item.checked = true;
            item.graduationDate = "Present";
        } else {
            console.log("checked is false");
            item.checked = false;
            item.graduationDate = "";
        }
    }

    ngOnInit(): void {}
}
