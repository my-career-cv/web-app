export const DATA = [
    {
        header: "Personal Info",
        title: "Let's start with your contact details",
        subTitle:
            "First, include your full name and contact details (home address is optional).",
        firstName: "",
        lastName: "",
        email: "",
        phone: "",
        jobTitle: "",
        image: "",
        website: "",
        address: ""
    },
    {
        header: "Education",
        title: "List your education",
        subTitle:
            "If you have higher education, skip your high school. Still studying? Include it too!",
        items: [
            {
                header: "School 1",
                schoolName: "",
                city: "",
                degree: "",
                fieldOfStudy: "",
                grade: "",
                startDate: "",
                graduationDate: "",
                checked: false
            }
        ]
    },
    {
        header: "Experience",
        title: "Describe your work experience",
        subTitle:
            "Start with your current or most recent job and work backward from there. Did Internships? Include it too!",
        items: [
            {
                header: "Position 1",
                employer: "",
                city: "",
                designation: "",
                startDate: "",
                endDate: "",
                description: "",
                checked: false
            }
        ]
    },
    {
        header: "Skills",
        title: "Highlight your key skills",
        subTitle:
            "Focus on what's most important. Aim for up to 8 skills. Use the rating scale to grab attention.",
        items: []
    },
    {
        header: "Summary",
        title: "Experience, education, skills—done! Time for the summary.",
        subTitle: "Describe 2–3 key skills and achievements.",
        summary: ""
    }
    // {
    //     header: "Finalize Resume",
    //     title: "Congratulations, your resume is ready!",
    //     subTitle:
    //         "Download your document and land the dream job.\nYou can still make changes if you need.",
    //     items: []
    // }
];

export const COLORS = [
    {
        color: "red",
        rgba: "rgb(244, 67, 54)"
    },
    {
        color: "pink",
        rgba: "rgb(233, 30, 99)"
    },
    {
        color: "purple",
        rgba: "rgb(156, 39, 176)"
    },
    {
        color: "deep purple",
        rgba: "rgb(103, 58, 183)"
    },
    {
        color: "indigo",
        rgba: "rgb(63, 81, 181)"
    },
    {
        color: "blue",
        rgba: "rgb(33, 150, 243)"
    },
    {
        color: "light blue",
        rgba: "rgb(3, 169, 244)"
    },
    {
        color: "cyan",
        rgba: "rgb(0, 188, 212)"
    },
    {
        color: "teal",
        rgba: "rgb(0, 150, 136)"
    },
    {
        color: "green",
        rgba: "rgb(76, 175, 80)"
    },
    {
        color: "light green",
        rgba: "rgb(139, 195, 74)"
    },
    {
        color: "lime",
        rgba: "rgb(205, 220, 57)"
    },
    {
        color: "yellow",
        rgba: "rgb(255, 235, 59)"
    },
    {
        color: "amber",
        rgba: "rgb(255, 193, 7)"
    },
    {
        color: "orange",
        rgba: "rgb(255, 152, 0)"
    },
    {
        color: "deep orange",
        rgba: "rgb(255, 87, 34)"
    },
    {
        color: "brown",
        rgba: "rgb(121, 85, 72)"
    },
    {
        color: "blue grey",
        rgba: "rgb(96, 125, 139)"
    }
];

export const SECTIONS = [
    {
        name: "Personal Info",
        title: "Let's start with your contact details",
        sub_title:
            "First, include your full name and contact details (home address is optional).",
        properties: [
            {
                name: "First Name",
                value: "",
                placeholder: "Ishaan"
            },
            {
                name: "Last Name",
                value: "",
                placeholder: "Desai"
            },
            {
                name: "Email ID",
                value: "",
                placeholder: "ishaan@abc.com"
            },
            {
                name: "Phone",
                value: "",
                placeholder: "9900887765"
            },
            {
                name: "Job Title",
                value: "",
                placeholder: "Your desired Job Title"
            },
            {
                name: "Address",
                value: "",
                placeholder: "#13, Maruthi Nagar Bellary Karnataka"
            }
        ]
    },
    {
        name: "Education",
        title: "List your education",
        sub_title:
            "If you have higher education, skip your high school. Still studying? Include it too!",
        properties: [
            {
                degree_name: "Degree 1",
                degree_details: [
                    {
                        name: "School Name",
                        value: "",
                        placeholder: "Visvesvaraya Technological University"
                    },
                    {
                        name: "City",
                        value: "",
                        placeholder: "Bengaluru"
                    },
                    {
                        name: "Degree",
                        value: "",
                        placeholder: "Bachelor of Engineering"
                    },
                    {
                        name: "Feild of Study",
                        value: "",
                        placeholder: "Civil Engineering"
                    },
                    {
                        name: "Grade",
                        value: "",
                        placeholder: "7.2 0r 80%"
                    },
                    {
                        name: "Start Date",
                        value: "",
                        placeholder: "YYYY-MM"
                    },
                    {
                        name: "Graduation Date",
                        value: "",
                        placeholder: "YYYY-MM"
                    }
                ]
            }
        ]
    },
    {
        name: "Work History",
        title: "Describe your work experience",
        sub_title:
            "Start with your current or most recent job and work backward from there.",
        properties: []
    },
    {
        name: "Skills",
        title: "Highlight your key skills",
        sub_title:
            "Focus on what's most important. Aim for up to 8 skills. Use the rating scale to grab attention.",
        properties: []
    },
    {
        name: "Summary",
        title: "Experience, education, skills—done! Time for the summary.",
        sub_title: "Describe 2–3 key skills and achievements.",
        properties: []
    },
    {
        name: "Finalize Resume",
        title: "Congratulations, your resume is ready!",
        sub_title:
            "Download your document and land the dream job.\nYou can still make changes if you need.",
        properties: []
    }
];
