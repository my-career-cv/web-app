import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { EditorRoutingModule } from "./editor-routing.module";
import { TemplateComponent } from "./template/template.component";
import { MatButtonModule } from "@angular/material/button";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatCardModule } from "@angular/material/card";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatListModule } from "@angular/material/list";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { FlexLayoutModule } from "@angular/flex-layout";
import { LayoutModule } from "@angular/cdk/layout";
import { MatTabsModule } from "@angular/material/tabs";
import { EnhancerComponent } from "./enhancer/enhancer.component";
import { EditComponent } from "./edit/edit.component";
import { EditPersonalComponent } from "./edit/edit-personal/edit-personal.component";
import { EditEducationComponent } from "./edit/edit-education/edit-education.component";
import { PreviewComponent } from "./preview/preview.component";
import { AsydeComponent } from "./template/asyde/asyde.component";
import { FormsModule } from "@angular/forms";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { ExperienceComponent } from "./edit/experience/experience.component";
import { SummaryComponent } from "./edit/summary/summary.component";
import { SkillsComponent } from "./edit/skills/skills.component";
import { MatMenuModule } from "@angular/material/menu";
import { MatSnackBarModule } from "@angular/material/snack-bar";

@NgModule({
    declarations: [
        TemplateComponent,
        EnhancerComponent,
        EditComponent,
        EditPersonalComponent,
        EditEducationComponent,
        PreviewComponent,
        AsydeComponent,
        ExperienceComponent,
        SummaryComponent,
        SkillsComponent
    ],
    imports: [
        CommonModule,
        EditorRoutingModule,
        MatButtonModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        MatSidenavModule,
        MatListModule,
        MatFormFieldModule,
        MatInputModule,
        FlexLayoutModule,
        LayoutModule,
        MatTabsModule,
        FormsModule,
        MatCheckboxModule,
        MatMenuModule,
        MatSnackBarModule
    ]
})
export class EditorModule {}
