import { Component, OnInit } from "@angular/core";
import { DATA } from "../data/SECTIONS";

@Component({
    selector: "app-preview",
    templateUrl: "./preview.component.html",
    styleUrls: ["./preview.component.scss"]
})
export class PreviewComponent implements OnInit {
    sections: any;
    constructor() {}

    ngOnInit(): void {
        this.sections = DATA;
    }
}
