import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";
import { Observable } from "rxjs";
import { map, shareReplay } from "rxjs/operators";
import { DATA } from "../data/SECTIONS";
import { COLORS } from "../data/SECTIONS";
import html2canvas from "html2canvas";
import * as jsPDF from "jspdf";
import { MatSnackBar } from "@angular/material/snack-bar";

@Component({
    selector: "app-enhancer",
    templateUrl: "./enhancer.component.html",
    styleUrls: ["./enhancer.component.scss"],
    encapsulation: ViewEncapsulation.None // required to overide mat-menu css
})
export class EnhancerComponent implements OnInit {
    public sections: any;
    colorsPalette: Array<any>;
    constructor(
        private breakpointObserver: BreakpointObserver,
        private _snackBar: MatSnackBar
    ) {}

    isHandset$: Observable<boolean> = this.breakpointObserver
        .observe([Breakpoints.Handset, Breakpoints.Tablet])
        .pipe(
            map(result => result.matches),
            shareReplay()
        );

    choseColor(index: number, item: any): void {
        console.log("selected color is ", item);

        let style = document.createElement("style");
        style.type = "text/css";
        style.innerHTML =
            ".custom-snackbar { background-color:" + item.rgba + "; }";
        document.getElementsByTagName("head")[0].appendChild(style);

        let textSecondary = document.getElementsByClassName(
            "text-secondary"
        ) as HTMLCollectionOf<HTMLElement>;
        console.log("textSeconday elements ", textSecondary);
        for (let i = 0; i < textSecondary.length; i++) {
            textSecondary[i].style.color = item.rgba;
        }

        this._snackBar.open("Color updated to " + item.color + "!", "Close", {
            duration: 3000,
            verticalPosition: "top",
            panelClass: ["custom-snackbar"]
        });
    }

    downloadCV(): void {
        let page = <HTMLDivElement>document.getElementById("page");
        // page.style.transform = "scale(1)";
        html2canvas(page, {
            scale: 5,
            useCORS: true,
            allowTaint: true
        }).then(canvas => {
            const image = canvas.toDataURL("image/jpeg");
            console.log("let's append the image", image);
            // document.body.appendChild(canvas);
            const doc = new jsPDF({
                orientation: "portrait",
                unit: "px",
                format: "a4"
            });
            // const pageWidth = doc.internal.pageSize.getWidth();
            // const pageHeight = doc.internal.pageSize.getHeight();
            // const widthRatio = pageWidth / canvas.width;
            // const heightRatio = pageHeight / canvas.height;
            // const ratio = widthRatio > heightRatio ? heightRatio : widthRatio;
            // const canvasWidth = canvas.width * ratio;
            // const canvasHeight = canvas.height * ratio;
            // let marginX = 0;
            // let marginY = 0;

            // doc.addImage(
            //     image,
            //     "JPEG",
            //     marginX,
            //     marginY,
            //     canvasWidth,
            //     canvasHeight,
            //     null,
            //     "SLOW"
            // );

            const pageHeight = doc.internal.pageSize.getHeight();
            const canvasWidth = doc.internal.pageSize.getWidth();
            const canvasHeight = (canvas.height * canvasWidth) / canvas.width;
            let marginTop = 0;
            let heightLeft = canvasHeight;
            doc.addImage(
                image,
                "JPEG",
                0,
                marginTop,
                canvasWidth,
                canvasHeight
            );
            heightLeft -= pageHeight;
            while (heightLeft >= 0) {
                marginTop = heightLeft - canvasHeight;
                doc.addPage();
                doc.addImage(
                    image,
                    "JPEG",
                    0,
                    marginTop,
                    canvasWidth,
                    canvasHeight
                );
                heightLeft -= pageHeight;
            }

            doc.save("mycareercv.pdf");
        });
    }

    ngOnInit(): void {
        this.sections = DATA;
        this.colorsPalette = COLORS;
    }
}
