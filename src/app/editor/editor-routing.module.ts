import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TemplateComponent } from "./template/template.component";
import { EnhancerComponent } from "./enhancer/enhancer.component";

const editorRoutes: Routes = [
    {
        path: "template",
        component: TemplateComponent
    },
    {
        path: "enhancer",
        component: EnhancerComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(editorRoutes)],
    exports: [RouterModule]
})
export class EditorRoutingModule {}
