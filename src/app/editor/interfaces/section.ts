export interface Section {
    header: string;
    title: string;
    subTitle: string;
    properties?: any[];
}

export interface Property {
    name: string;
    value: string;
    placeholder: string;
}

export interface Personal {
    header: string;
    title: string;
    subTitle: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    jobTitle: string;
    image: string;
    website: string;
    address: string;
}

export interface Education {
    header: string;
    title: string;
    subTitle: string;
    items: [
        {
            header: string;
            schoolName: string;
            city: string;
            degree: string;
            fieldOfStudy: string;
            grade: string;
            startDate: string;
            graduationDate: string;
            checked: boolean;
        }
    ];
}
export interface Experience {
    header: string;
    title: string;
    subTitle: string;
    items: [
        {
            header: string;
            employer: string;
            city: string;
            designation: string;
            startDate: string;
            endDate: string;
            checked: boolean;
            description: string;
        }
    ];
}

export interface Summary {
    header: string;
    title: string;
    subTitle: string;
    summary: string;
}

export interface Skills {
    header: string;
    title: string;
    subTitle: string;
    items: string[];
}
