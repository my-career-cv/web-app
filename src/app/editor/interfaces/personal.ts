export interface Personal {
    header: string;
    title: string;
    subTitle: string;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    jobTitle: string;
    image: string;
    website: string;
    address: string;
}
